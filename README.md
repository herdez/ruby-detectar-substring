# Ejercicio - Detectar Substring

Crea el método `shipping` que recibe como parámetro `address`.

Si el `address` incluye `Mexico` regresa `"Order received"`, para cualquier otro caso regresa `"We only ship orders to Mexico"`.

```ruby
#shipping method


#driver code

p shipping('Insurgentes Sur 8932, Alvaro Obregon, Mexico') == "Order received"
p shipping('Insurgentes Sur 8932, Alvaro Obregon, MÈxIcO') == "Order received"
p shipping('Geary Blvd 3320, San Francisco, Estados Unidos') == "We only ship orders to Mexico"
```